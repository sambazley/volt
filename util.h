#ifndef UTIL_H
#define UTIL_H

int base64_encode_length(int srclen);
void base64_encode(char *dest, char *src, int destlen, int srclen);

#endif /* UTIL_H */
