#include "flash.h"
#include <os_type.h>
#include <osapi.h>
#include <mem.h>

char ICACHE_FLASH_ATTR flashReadByte(const char *addr) {
    long chunk = *(long *)((long) addr & ~3);
    return ((char *) &chunk)[(long) addr & 3];
}

int ICACHE_FLASH_ATTR flashStrlen(const char *addr) {
    const char *start = addr;
    while (flashReadByte(addr)) {
        addr++;
    }
    return addr-start;
}

void ICACHE_FLASH_ATTR flashMemcpy(void *dest, const void *src, long n) {
    int startBytes = (4 - (long) src & 3) & 3;
    int endBytes = ((long) src+n) & 3;

    if (n < startBytes) {
        startBytes = n;
        endBytes = 0;
    }

    for (int i = 0; i < startBytes; i++) {
        *(char *)(dest + i) = flashReadByte(src + i);
    }

    for (int i = 0; i < n-startBytes-endBytes; i+=4) {
        int chunk = *(long *)(((long)src+startBytes+i) & ~3);
        *(char *)(dest+startBytes+i+0) = *((char *) &chunk+0);
        *(char *)(dest+startBytes+i+1) = *((char *) &chunk+1);
        *(char *)(dest+startBytes+i+2) = *((char *) &chunk+2);
        *(char *)(dest+startBytes+i+3) = *((char *) &chunk+3);
    }

    for (int i = 0; i < endBytes; i++) {
        *(char *)(dest+n-endBytes+i) = flashReadByte(src+n-endBytes+i);
    }
}
