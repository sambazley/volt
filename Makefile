CC = xtensa-lx106-elf-gcc
CFLAGS = -std=c99 -I. -Iinclude -mlongcalls -DICACHE_FLASH -Os
LDLIBS = -nostdlib -Wl,--start-group -lmain -lnet80211 -lwpa -llwip -lpp -lphy -lc -Wl,--end-group -lgcc -ldriver -mtext-section-literals
LDFLAGS = -Teagle.app.v6.ld
include Network.mk

#CFLAGS += -I../gdbstub -Og -ggdb
#LDLIBS += -L../gdbstub -lgdbstub

esp-0x00000.bin: esp
	esptool elf2image $^

esp: adc.o esp.o flash.o filesystem.o fs.o http.o util.o websocket.o wifi.o

adc.o: adc.c

esp.o: esp.c

flash.o: flash.c

filesystem.o: filesystem.c

fs.o: fs.bin fs.S

fs.bin: fs/*
	./makefs.sh

http.o: http.c

util.o: util.c

websocket.o: websocket.c

wifi.o: wifi.c

flash: esp-0x00000.bin
	esptool -p /dev/ttyUSB0 --baud 230400 write_flash 0 esp-0x00000.bin 0x10000 esp-0x10000.bin

clean:
	rm -f esp *.o esp-0x*.bin fs.bin
