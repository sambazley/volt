#include "adc.h"
#include "websocket.h"
#include <mem.h>
#include <os_type.h>
#include <osapi.h>

static struct {
    unsigned char samplesCount;
    unsigned char bufferSize;
    unsigned char sampleRate;
} settings;

static volatile os_timer_t pollTimer;

static char *buffer = 0;

static void ICACHE_FLASH_ATTR adc() {
    static int i = 0;

    if (i >= settings.bufferSize) {
        i = 0;

        websocketSendAll(WS_BINARY_FRAME, buffer, settings.bufferSize*5/4);

        for (int j = 0; j < settings.bufferSize*5/4; j++) {
            buffer[j] = 0;
        }
    }

    unsigned int adc = 0;

    for (int j = 0; j < settings.samplesCount; j++) {
        int r = system_adc_read();
        adc += (r == 1024 ? 1023 : r);
        os_delay_us(100);
    }

    adc /= settings.samplesCount;

    int byteInSec = i % 4;

    buffer[i/4*5] |= ((adc >> 8) & 0x03) << (6-byteInSec*2);
    buffer[i+i/4+1] = adc & 0xFF;

    i++;
}

static void ICACHE_FLASH_ATTR updateSettings(int obs) {
    if (settings.bufferSize < 4) {
        settings.bufferSize = 4;
    }
    if (settings.bufferSize % 4 != 0) {
        settings.bufferSize = settings.bufferSize/4*4;
    }

    if (settings.sampleRate == 0) {
        settings.sampleRate = 1;
    }

    if (settings.samplesCount == 0) {
        settings.samplesCount = 1;
    }
    if (settings.samplesCount > 10) {
        settings.samplesCount = 10;
    }

    os_printf("bufferSize: %u\n", settings.bufferSize);
    os_printf("samplesCount: %u\n", settings.samplesCount);
    os_printf("sampleRate: %u\n", settings.sampleRate);

    if (buffer) {
        buffer = os_realloc(buffer, settings.bufferSize*5/4);
        for (int i = obs*5/4; i < settings.bufferSize*5/4; i++) {
            *(buffer+i) = 0;
        }
    } else {
        buffer = os_malloc(settings.bufferSize*5/4);
    }

    os_timer_disarm(&pollTimer);
    os_timer_setfn(&pollTimer, (os_timer_func_t *) adc, NULL);
    os_timer_arm(&pollTimer, 1000/settings.sampleRate, 1);
}

static void ICACHE_FLASH_ATTR sendSettings(struct WebSocket *ws) {
    char data [3] = {
        settings.bufferSize,
        settings.samplesCount,
        settings.sampleRate
    };

    if (ws == 0) {
        websocketSendAll(WS_BINARY_FRAME, data, 3);
    } else {
        websocketSend(ws, WS_BINARY_FRAME, data, 3);
    }
}

static void ICACHE_FLASH_ATTR onConnect(struct WebSocket *ws) {
    sendSettings(ws);
}

static void ICACHE_FLASH_ATTR
onReceive(char *data, int len, struct WebSocket *ws) {
    if (*data == 0x00 && len == 4) {
        int obs = settings.bufferSize;
        settings.bufferSize = *(data + 1);
        settings.samplesCount = *(data + 2);
        settings.sampleRate = *(data + 3);
        updateSettings(obs);

        sendSettings(0);

        return;
    }
}

void ICACHE_FLASH_ATTR adc_init() {
    websocketRegistConncb(onConnect);
    websocketRegistRecvcb(onReceive);

    settings.samplesCount = 10;
    settings.bufferSize = 12;
    settings.sampleRate = 64;

    updateSettings(0);
}
