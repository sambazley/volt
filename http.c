#include "http.h"
#include "filesystem.h"
#include "websocket.h"
#include "wifi.h"
#include <mem.h>
#include <os_type.h>
#include <osapi.h>
#include <user_interface.h>
#include <espconn.h>

static void ICACHE_FLASH_ATTR
*parseHeader(char *data, char **method, char **page, struct HeaderField **base){
    *method = strtok(data, "\r");

    struct HeaderField **hf = base;
    char *field = strtok(0, "\r");
    while (field) {
        field++; //skip '\n'
        if (*field == '\0') break;

        (*hf) = (struct HeaderField *) os_malloc(sizeof(struct HeaderField));
        (*hf)->next = 0;

        (*hf)->key = field;

        char *del = strchr(field, ':');
        *del = '\0'; //replace ':' with end of key

        (*hf)->val = del+1;

        while (*(*hf)->val == ' ') { //skip any spaces after ':'
            (*hf)->val++;
        }

        hf = &(*hf)->next;

        field = strtok(0, "\r");
    }

    *method = strtok(*method, " ");
    *page = strtok(0, " ");
}

static int ICACHE_FLASH_ATTR
hfKeyValMatch(struct HeaderField *hf, char *key, char *val) {
    while (hf) {
        if (strcmp(hf->key, key) == 0 && strcmp(hf->val, val) == 0) {
            return 1;
        }
        hf = hf->next;
    }
    return 0;
}

static void
ICACHE_FLASH_ATTR onReceive(void *arg, char *data, unsigned short len) {
    if (strncmp(data, "GET", 3)) {
        return;
    }

    struct espconn *conn = (struct espconn *) arg;

    struct HeaderField *bHField;
    char *method;
    char *page;

    parseHeader(data, &method, &page, &bHField);

    if (strcmp(page, "/") == 0) {
        page = "/index.html";
    }

    if (hfKeyValMatch(bHField, "Upgrade", "websocket")) {
        websocket(conn, bHField);
        return;
    }

    os_printf("%s %s\n", method, page);

    char *hFmt = "\
HTTP/1.1 200 OK\r\n\
Server: ESP8266\r\n\
Content-Length: %u\r\n\
\r\n";

    if (!filesystemFileExists(page)) {
             hFmt = "\
HTTP/1.1 404 Not Found\r\n\
Server: ESP8266\r\n\
Content-Length: %u\r\n\
\r\n";

       if (filesystemFileExists("/404.html")) {
            page = "/404.html";
        } else {
            char *pageData = "404 Page Not Found";
            char *header = os_malloc(strlen(hFmt) + 10);
            os_sprintf(header, hFmt, strlen(pageData));

            wifiBufferPushText(conn, header, strlen(header));
            wifiBufferPushText(conn, pageData, strlen(pageData));

            os_free(header);

            return;
        }
    }

    char *header = os_malloc(strlen(hFmt) + 10);
    os_sprintf(header, hFmt, filesystemGetFileSize(page));

    wifiBufferPushText(conn, header, strlen(header));
    wifiBufferPushFile(conn, page);

    os_free(header);
}

static void ICACHE_FLASH_ATTR onDisconnect(void *arg) {
    struct espconn *conn = (struct espconn *) arg;
    wifiRemoveBuffersByConn(conn->reverse);
}

static void ICACHE_FLASH_ATTR onConnect(void *arg) {
    struct espconn *conn = (struct espconn *) arg;
    conn->reverse = (void *) conn;
    espconn_regist_recvcb(conn, onReceive);
    espconn_regist_disconcb(conn, onDisconnect);
}


void ICACHE_FLASH_ATTR http_init() {
    extern struct espconn server;
    espconn_regist_connectcb(&server, onConnect);
}
