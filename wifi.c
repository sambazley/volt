#include "wifi.h"
#include "filesystem.h"
#include <os_type.h>
#include <osapi.h>
#include <user_interface.h>
#include <espconn.h>
#include <mem.h>

#define SENDSIZE 256

#define MAXBUFFEREDBYTES 512

struct espconn server;

enum BufferType {
    FILEBUFFER,
    TEXTBUFFER
};

struct BufferObject {
    enum BufferType type;

    struct espconn *conn;
    char *data;

    int size;
    int sent;

    struct BufferObject *prev;
    struct BufferObject *next;
};

static struct BufferObject *baseBuffer;
static struct BufferObject *lastBuffer;

static struct espconn *sending = 0;

static void ICACHE_FLASH_ATTR removeBuffer(struct BufferObject *o) {
    if (o == baseBuffer) {
        baseBuffer = o->next;
    }
    if (o == lastBuffer) {
        lastBuffer = o->prev;
    }

    if (o->prev) {
        o->prev->next = o->next;
    }
    if (o->next) {
        o->next->prev = o->prev;
    }

    os_free(o->data);
    os_free(o);
}

static void ICACHE_FLASH_ATTR sendBaseBuffer() {
    struct BufferObject *o = baseBuffer;
    if (o->type == FILEBUFFER) {
        int rem = filesystemGetFileSize(o->data) - o->sent;
        int sendSize = rem > SENDSIZE ? SENDSIZE : rem;

        char *data = os_malloc(sendSize);
        filesystemCopyFileData(o->data, data, o->sent, sendSize);
        sending = o->conn;
        int r = espconn_send(o->conn, data, sendSize);

        switch (r) {
            case ESPCONN_MEM:
                os_printf("ESPCONN_MEM\n");
                break;
            case ESPCONN_ARG:
                os_printf("ESPCONN_ARG\n");
                break;
            case ESPCONN_MAXNUM:
                os_printf("ESPCONN_MAXNUM\n");
                break;
        }

        os_free(data);

        o->sent += sendSize;

        if (rem - sendSize == 0) {
            removeBuffer(o);
        }
    } else if (o->type == TEXTBUFFER) {
        int rem = o->size - o->sent;
        int sendSize = rem > SENDSIZE ? SENDSIZE : rem;

        sending = o->conn;

        int r = espconn_send(o->conn, o->data+o->sent, sendSize);

        switch (r) {
            case ESPCONN_MEM:
                os_printf("ESPCONN_MEM\n");
                break;
            case ESPCONN_ARG:
                os_printf("ESPCONN_ARG\n");
                break;
            case ESPCONN_MAXNUM:
                os_printf("ESPCONN_MAXNUM\n");
                break;
        }

        o->sent += sendSize;

        if (rem - sendSize == 0) {
            removeBuffer(o);
        }
    }
}

void ICACHE_FLASH_ATTR wifiRemoveBuffersByConn(struct espconn *conn) {
    struct BufferObject *o = baseBuffer;

    while (o) {
        struct BufferObject *next = o->next;
        if (o->conn == conn) {
            removeBuffer(o);
        }

        o = next;
    }

    if (sending == conn) {
        sending = 0;
    }
}

static int ICACHE_FLASH_ATTR getTotalBufferSizeByConn(struct espconn *conn) {
    int i = 0;

    struct BufferObject *o = baseBuffer;

    while (o) {
        if (o->conn == conn) {
            i += o->size;
        }
        o = o->next;
    }

    return i;
}

void ICACHE_FLASH_ATTR
wifiBufferPushText(struct espconn *conn, char *text, int size) {
    if (getTotalBufferSizeByConn(conn) > MAXBUFFEREDBYTES) {
        return;
    }

    struct BufferObject *o = os_malloc(sizeof(struct BufferObject));
    o->type = TEXTBUFFER;
    o->conn = conn;
    o->data = os_malloc(size);
    os_memcpy(o->data, text, size);

    o->size = size;
    o->sent = 0;

    o->prev = 0;
    o->next = 0;

    if (lastBuffer) {
        o->prev = lastBuffer;
        lastBuffer->next = o;
    }

    lastBuffer = o;

    if (!baseBuffer) {
        baseBuffer = o;
    }

    if (!sending) {
        sendBaseBuffer();
    }
}

void ICACHE_FLASH_ATTR wifiBufferPushFile(struct espconn *conn, char *name) {
    if (getTotalBufferSizeByConn(conn) > MAXBUFFEREDBYTES) {
        return;
    }

    struct BufferObject *o = os_malloc(sizeof(struct BufferObject));
    o->type = FILEBUFFER;
    o->conn = conn;
    o->data = os_malloc(strlen(name) + 1);
    strcpy(o->data, name);

    o->size = 0;
    o->sent = 0;

    o->prev = 0;
    o->next = 0;

    if (lastBuffer) {
        o->prev = lastBuffer;
        lastBuffer->next = o;
    }

    lastBuffer = o;

    if (!baseBuffer) {
        baseBuffer = o;
    }

    if (!sending) {
        sendBaseBuffer();
    }
}

static void ICACHE_FLASH_ATTR sent(void *arg) {
    struct espconn *conn = (struct espconn *) arg;

    sending = 0;

    if (baseBuffer) {
        sendBaseBuffer();
    }
}

void ICACHE_FLASH_ATTR wifi_init() {
    espconn_tcp_set_max_con(8);

    wifi_set_opmode(0x01);
    wifi_station_set_hostname("volt");

    struct station_config stationConf;
    stationConf.bssid_set = 0;
    os_memcpy(&stationConf.ssid, SSID, 32);
    os_memcpy(&stationConf.password, PASSWORD, 64);
    wifi_station_set_config(&stationConf);

    server.type = ESPCONN_TCP;
    server.state = ESPCONN_NONE;
    server.proto.tcp = (esp_tcp *) os_zalloc(sizeof(esp_tcp));
    server.proto.tcp->local_port = 80;

    espconn_regist_sentcb(&server, sent);
    espconn_accept(&server);
    espconn_regist_time(&server, 100, 0);

    baseBuffer = 0;
    lastBuffer = 0;
}
