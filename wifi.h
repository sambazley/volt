#ifndef WIFI_H
#define WIFI_H

#include <os_type.h>
#include <user_interface.h>
#include <espconn.h>

void wifiRemoveBuffersByConn(struct espconn *conn);
void wifiBufferPushText(struct espconn *conn, char *text, int size);
void wifiBufferPushFile(struct espconn *conn, char *name);
void wifi_init();

#endif /* WIFI_H */
