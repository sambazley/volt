#ifndef FILESYSTEM_H
#define FILESYSTEM_H

void filesystem_init();
void filesystemCopyFileData(char *fname, void *dest, int off, int n);
long filesystemGetFileSize(char *fname);
int filesystemFileExists(char *fname);

#endif /* FILESYSTEM_H */
