#include "util.h"
#include <c_types.h>

static const char b64tab [] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                               'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                               'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                               'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                               'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                               'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                               'w', 'x', 'y', 'z', '0', '1', '2', '3',
                               '4', '5', '6', '7', '8', '9', '+', '/'};

int ICACHE_FLASH_ATTR base64_encode_length(int srclen) {
    return ((srclen + 2) / 3) * 4;
}

void ICACHE_FLASH_ATTR
base64_encode(char *dest, char *src, int destlen, int srclen) {
    for (int i = 0; i < srclen; i += 3) {
        char a = i + 0 >= srclen ? 0 : *(src + i + 0);
        char b = i + 1 >= srclen ? 0 : *(src + i + 1);
        char c = i + 2 >= srclen ? 0 : *(src + i + 2);

        int w = (a & 0xFC) >> 2;
        int x = (a & 0x03) << 4 | (b & 0xF0) >> 4;
        int y = (b & 0x0F) << 2 | (c & 0xC0) >> 6;
        int z = (c & 0x3F);

        *(dest + i / 3 * 4 + 0) = b64tab[w];
        *(dest + i / 3 * 4 + 1) = b64tab[x];
        *(dest + i / 3 * 4 + 2) = b64tab[y];
        *(dest + i / 3 * 4 + 3) = b64tab[z];
    }

    for (int i = 0; i < 3- (srclen % 3); i++) {
        *(dest + destlen - i - 1) = '=';
    }
}
