#!/bin/bash

[ -f fs.bin ] && rm fs.bin

while read -u 3 line; do
    echo -ne $line | sed 's/fs//g' >> fs.bin
    printf "\x0" >> fs.bin

    printf "%08x" $(du -b $line | cut -f1) | xxd -r -p >> fs.bin

    cat $line >> fs.bin
done 3<<< $(find fs -not -type d)

printf "\0" >> fs.bin
