#ifndef HTTP_H
#define HTTP_H

struct HeaderField {
    char *key;
    char *val;

    struct HeaderField *next;
};

void http_init();

#endif /* HTTP_H */
