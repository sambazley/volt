var canv;
var ctx;
var width;
var height;
var axes;
var padding;

var socket;

var data = [];

var maxV = 3.33;

var xdiff = 1;
var ydivs = 4;

var connected = false;
var respTimer;

var gTime;

var max;
var min;
var avg;

function log(x) {
    var dStr = new Date().toTimeString().split(" ")[0];
    document.getElementById("log").innerHTML += "["+dStr+"] " + x + "<br />";
}

function rawToY(x) {
    return padding[2]-x/1024*(padding[2]-padding[1]);
}

function renderData() {
    ctx.putImageData(axes, 0, 0);

    ctx.beginPath();

    for (var i = 0; i < data.length; i++) {
        var y = rawToY(data[i]);
        if (i == 0) {
            ctx.moveTo(i*xdiff+padding[0], y);
        } else {
            ctx.lineTo(i*xdiff+padding[0], y);
        }
    }

    ctx.stroke();

    if (document.getElementById("renderStats").checked) {
        ctx.beginPath();
        ctx.moveTo(padding[0], rawToY(min));
        ctx.lineTo(padding[3], rawToY(min));
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(padding[0], rawToY(avg));
        ctx.lineTo(padding[3], rawToY(avg));
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(padding[0], rawToY(max));
        ctx.lineTo(padding[3], rawToY(max));
        ctx.stroke();
    }

    ctx.fillText(gTime, padding[3]-20, padding[2]+10);
}

function stats() {
    min = 1024;
    max = avg = 0;

    for (var i = 0; i < data.length; i++) {
        if (data[i] > max) {
            max = data[i];
        }

        if (data[i] < min) {
            min = data[i];
        }

        avg += data[i];
    }

    avg /= data.length;

    document.getElementById("stats").innerHTML =
        "Min: " + (min / 1024 * maxV).toFixed(2) + "V; " +
        "Max: " + (max / 1024 * maxV).toFixed(2) + "V; " +
        "Avg: " + (avg / 1024 * maxV).toFixed(2) + "V";
}

function gotData(ba) {
    for (var i = 0; i < ba.length*8/10; i++) {
        var byteInSec = i%4;

        var cData = ((ba[Math.floor(i/4)*5] << ((byteInSec+1)*2)) & 0x300)
                  | ba[i+Math.floor(i/4)+1];

        data.push(cData);

        if (data.length > width / xdiff) {
            data.shift();
        }
    }

    stats();

    renderData();

    clearTimeout(respTimer);
    respTimer = setTimeout(function() {
        socket.close();
        connected = false;
        log("Disconnected");
        connect();
    }, 10000);
}

function connect() {
    socket = new WebSocket("ws://"+window.location.hostname);

    socket.binaryType = 'arraybuffer';

    socket.onmessage = function (event) {
        if (!connected) {
            log("Connected");
            connected = true;
        }

        var ba = new Uint8Array(event.data);

        if (ba.length == 3) {
            document.getElementById("bufferSize").value = ba[0];
            document.getElementById("samplesCount").value = ba[1];
            document.getElementById("sampleRate").value = ba[2];

            gTime = (padding[3]-padding[0])/xdiff/ba[2];
        } else {
            gotData(ba);
        }
    };
}

window.onload = function() {
    canv = document.getElementById("canv");
    ctx  = canv.getContext("2d");
    ctx.translate(0.5, 0.5);

    width = canv.width;
    height = canv.height;

    padding = [25, 5, height-30, width-5];

    ctx.beginPath();
    ctx.moveTo(padding[0], padding[1]);
    ctx.lineTo(padding[0], padding[2]);
    ctx.lineTo(padding[3], padding[2]);
    ctx.stroke();

    ctx.save();

    ctx.font = "8px Mono";
    for (var i = 0; i <= maxV*ydivs; i++) {
        var y = Math.round(padding[2]-(padding[2]-padding[1])/(maxV*ydivs)*i);
        if ((i % ydivs) == 0) {
            ctx.fillText(i/ydivs, padding[0]-10, y+3);
            ctx.setLineDash([1, 4]);
            ctx.strokeStyle="#777777";
        } else {
            ctx.setLineDash([1, 6]);
            ctx.strokeStyle="AAAAAA";
        }

        ctx.beginPath();
        ctx.moveTo(padding[0], y);
        ctx.lineTo(padding[3], y);
        ctx.stroke();
    }

    ctx.restore();

    axes = ctx.getImageData(0, 0, width, height);

    document.getElementById("apply").addEventListener("click", function() {
        var bs = document.getElementById("bufferSize").value;
        var sc = document.getElementById("samplesCount").value;
        var sr = document.getElementById("sampleRate").value;
        if (connected) {
            var bytes = new Uint8Array([0x00, bs, sc, sr]);
            socket.send(bytes.buffer);
        }
        gTime = (padding[3]-padding[0])/xdiff/sr;
    });

    connect();
};
