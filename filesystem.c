#include "filesystem.h"
#include "flash.h"
#include <mem.h>
#include <os_type.h>
#include <osapi.h>

struct File {
    char *name;
    char *data;
    long size;

    struct File *next;
};

static struct File *base;

static char ICACHE_FLASH_ATTR *getFileName(struct File *file) {
    char *addr = file->name;
    int len = flashStrlen(addr);

    char *name = os_malloc(len+1);
    flashMemcpy(name, addr, len+1);
    return name;
}

void ICACHE_FLASH_ATTR filesystem_init() {
    extern char _binary_fs_bin_start;

    char *data = &_binary_fs_bin_start;

    struct File **file = &base;

    while (flashReadByte(data)) {
        *file = (struct File *) os_malloc(sizeof(struct File));
        (*file)->next = 0;

        (*file)->name = data;

        data += flashStrlen(data) + 1;

        (*file)->size = (flashReadByte(data+0) << 24
                       | flashReadByte(data+1) << 16
                       | flashReadByte(data+2) <<  8
                       | flashReadByte(data+3));

        char *name = getFileName(*file);
        os_printf("%s: %u\n", name, (*file)->size);
        os_free(name);

        data += 4;

        (*file)->data = data;

        data += (*file)->size;

        file = &((*file)->next);
    }
}

static struct File ICACHE_FLASH_ATTR *getFileByName(char *fname) {
    struct File *file = base;

    while (file) {
        char *nameTest = getFileName(file);
        if (strcmp(nameTest, fname) == 0) {
            os_free(nameTest);
            return file;
        }
        os_free(nameTest);
        file = file->next;
    }
}

void ICACHE_FLASH_ATTR
filesystemCopyFileData(char *fname, void *dest, int off, int n) {
    struct File *file = getFileByName(fname);
    flashMemcpy(dest, file->data+off, n);
}

long ICACHE_FLASH_ATTR filesystemGetFileSize(char *fname) {
    struct File *file = getFileByName(fname);
    return file->size;
}

int ICACHE_FLASH_ATTR filesystemFileExists(char *fname) {
    struct File *file = base;

    while (file) {
        char *nameTest = getFileName(file);
        if (strcmp(nameTest, fname) == 0) {
            os_free(nameTest);
            return true;
        }
        os_free(nameTest);
        file = file->next;
    }

    return false;
}
