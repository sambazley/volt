#ifndef FLASH_H
#define FLASH_H

char flashReadByte(const char *addr);
int flashStrlen(const char *addr);
void flashMemcpy(void *dest, const void *src, long n);

#endif /* FLASH_H */
