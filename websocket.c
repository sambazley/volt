#include "websocket.h"
#include "util.h"
#include "wifi.h"
#include <os_type.h>
#include <osapi.h>
#include <user_interface.h>
#include <espconn.h>
#include <mem.h>

#define MAXCONNECTIONS 4

struct SHA1_CTX {
    long state [5];
    long count [2];
    unsigned char buffer [64];
};

static struct espconn *connections [MAXCONNECTIONS] = {0};

static void (*conncb)(struct WebSocket *ws) = 0;
static void (*recvcb)(char *data, int len, struct WebSocket *ws) = 0;

void ICACHE_FLASH_ATTR
websocketSend(struct WebSocket *ws, char opcode, char *data, int len) {
    struct espconn *conn = ws->conn;
    char *frame = os_malloc(4+len);
    *(frame + 0) = 1 << 7 //FIN
                 | 0 << 6 //RSV
                 | 0 << 5 //RSV
                 | 0 << 4 //RSV
                 | opcode;

    if (len >= 126) {
        *(frame + 1) = 0 << 7 //MASK
                     | 126;

        *(frame + 2) = len >> 8;
        *(frame + 3) = len & 0xFF;

        os_memcpy(frame + 4, data, len);
        wifiBufferPushText(conn, frame, 4+len);
    } else {
        *(frame + 1) = 0 << 7 //MASK
                     | len & 0x7F; //length

        os_memcpy(frame + 2, data, len);

        wifiBufferPushText(conn, frame, 2+len);
    }

    os_free(frame);
}

void ICACHE_FLASH_ATTR websocketSendAll(char opcode, char *data, int len) {
    for (int i = 0; i < MAXCONNECTIONS; i++) {
        struct espconn *conn = connections[i];
        if (conn) {
            websocketSend(conn->reverse, opcode, data, len);
        }
    }
}

void ICACHE_FLASH_ATTR websocketRegistConncb(void *cb) {
    conncb = cb;
}

void ICACHE_FLASH_ATTR websocketRegistRecvcb(void *cb) {
    recvcb = cb;
}

static void ICACHE_FLASH_ATTR
onReceive(void *arg, char *data, unsigned short len) {
    struct espconn *conn = (struct espconn *) arg;
    struct WebSocket *ws = (struct WebSocket *) conn->reverse;

    int fin    = (*data >> 7) & 1;
    int rsv1   = (*data >> 6) & 1;
    int rsv2   = (*data >> 6) & 1;
    int rsv3   = (*data >> 4) & 1;

    int opcode = *data & 0x0F;

    data++;

    if (opcode == WS_CLOSE_FRAME) {
        os_printf("Websocket %u disconnected\n", ws->slot);
        connections[ws->slot] = 0;
        ws->disconnect = 1;
        websocketSend(ws, WS_CLOSE_FRAME, 0, 0);

        return;
    }

    int mask   = (*data >> 7) & 1;
    int pldLen = (*data >> 0) & 0x7F;

    data++;

    if (!mask) {
        os_printf("Mask bit not set\n");
        espconn_disconnect(ws->conn);
    }

    if (pldLen == 126) {
        pldLen = *data++ << 8 | *data++;
    } else if (pldLen == 127) {
        os_printf("Payload length too long\n");
        espconn_disconnect(ws->conn);
    }

    char maskingKey [4];
    for (int i = 0; i < 4; i++) {
        maskingKey[i] = *data++;
    }

    for (int i = 0; i < pldLen; i++) {
        *(data + i) ^= maskingKey[i%4];
    }

    if (recvcb) {
        recvcb(data, pldLen, ws);
    }
}

static void ICACHE_FLASH_ATTR onDisconnect(void *arg) {
    struct espconn *conn = (struct espconn *) arg;

    struct WebSocket *ws = (struct WebSocket *) conn->reverse;

    if (!ws->disconnect) {
        os_printf("WebSocket %u disconnected unexpectedly\n", ws->slot);
        connections[ws->slot] = 0;
    }
    wifiRemoveBuffersByConn(ws->conn);
    os_free(ws);
}

void ICACHE_FLASH_ATTR websocket(struct espconn *conn, struct HeaderField *hfb) {
    for (int i = 0; i < MAXCONNECTIONS; i++) {
        if (connections[i] == 0) {

            os_printf("WebSocket %u connected\n", i);

            struct WebSocket *ws = os_malloc(sizeof(struct WebSocket));
            ws->slot = i;
            ws->conn = conn;
            ws->disconnect = 0;

            conn->reverse = ws;
            connections[i] = conn;
            goto notFull;
        }
    }

    os_printf("WebSocket connection refused, server full\n");
    espconn_disconnect(conn);
    return;

notFull:

    espconn_set_keepalive(conn, ESPCONN_KEEPIDLE, (void *) 5);
    espconn_set_opt(conn, ESPCONN_KEEPALIVE);
    espconn_regist_recvcb(conn, onReceive);
    espconn_regist_disconcb(conn, onDisconnect);

    char *hFmt = "\
HTTP/1.1 101 Switching Protocols\r\n\
Upgrade: websocket\r\n\
Connection: Upgrade\r\n\
Sec-WebSocket-Accept: %s\r\n\
\r\n";

    char *keyConcat;
    struct HeaderField *hf = hfb;
    while (hf) {
        if (strcmp(hf->key, "Sec-WebSocket-Key") == 0) {
            char *magic = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
            keyConcat = os_malloc(strlen(hf->val) + strlen(magic) + 1);
            strcpy(keyConcat, hf->val);
            strcpy(keyConcat+strlen(hf->val), magic);

            break;
        }
        hf = hf->next;
    }

    struct SHA1_CTX ctx;
    char hash [20];
    SHA1Init(&ctx);
    SHA1Update(&ctx, keyConcat, strlen(keyConcat));
    SHA1Final(hash, &ctx);

    int b64len = base64_encode_length(20);
    char *base64 = os_malloc(b64len + 1);
    base64_encode(base64, hash, b64len, 20);
    *(base64 + b64len) = '\0';

    char *header = os_malloc(strlen(hFmt)-2+b64len+1);
    os_sprintf(header, hFmt, base64);

    wifiBufferPushText(conn, header, strlen(header));

    os_free(keyConcat);
    os_free(base64);
    os_free(header);

    if (conncb) {
        conncb(conn->reverse);
    }
}
