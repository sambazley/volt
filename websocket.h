#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#include "http.h"
#include <os_type.h>
#include <user_interface.h>
#include <espconn.h>

#define WS_TEXT_FRAME 1
#define WS_BINARY_FRAME 2
#define WS_CLOSE_FRAME 8

struct WebSocket {
    int slot;
    struct espconn *conn;

    char disconnect:1;
};

void websocket(struct espconn *conn, struct HeaderField *hf);

void websocketSend(struct WebSocket *ws, char opcode, char *data, int len);
void websocketSendAll(char opcode, char *data, int len);
void websocketRegistConncb(void *cb);
void websocketRegistRecvcb(void *cb);

#endif /* WEBSOCKET_H */
