#include "adc.h"
#include "filesystem.h"
#include "http.h"
#include "wifi.h"
#include <user_interface.h>
#include <driver/uart.h>
//#include <gdbstub.h>

#include <osapi.h>
#include <mem.h>

void ICACHE_FLASH_ATTR user_init() {
    uart_init(BIT_RATE_115200, BIT_RATE_115200);

//    gdbstub_init();

    gpio_init();
    filesystem_init();
    wifi_init();
    http_init();
    adc_init();
}
